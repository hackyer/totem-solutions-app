package fooddeliveryapp.totemsolutions.com.fooddeliveryapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MakeProfileActivity extends AppCompatActivity {

    static final int DIALOG_ID = 0;
    static int PICK_IMAGE_REQUEST = 1;
    ImageView userImage;
    Bitmap userImgBitmap;
    Button saveInfoBtn;
    Spinner userGender;
    EditText userDOB, userName, userAddress, userEmail;
    int year_x, month_x, day_x;
    String gender;

    FirebaseAuth firebaseAuth;
    ProgressDialog progressDialog;

    DatabaseReference databaseReference;

    private DatePickerDialog.OnDateSetListener dpickerListerner
            = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            year_x = year;
            month_x = month + 1;
            day_x = day;

            userDOB.setText(day_x + "/" + month_x + "/" + year_x);

//            Toast.makeText(MakeProfileActivity.this, year_x + "/" + month_x + "/" + day_x, Toast.LENGTH_LONG).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_profile);

        final Calendar calendar = Calendar.getInstance();
        year_x = calendar.get(Calendar.YEAR);
        month_x = calendar.get(Calendar.MONTH);
        day_x = calendar.get(Calendar.DAY_OF_MONTH);

        showDialogOnButtonClick();

        userImage = (ImageView) findViewById(R.id.userImage);
        userImgBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.user_demo_image);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), userImgBitmap);
        roundedBitmapDrawable.setCircular(true);
        userImage.setImageDrawable(roundedBitmapDrawable);

        userGender = (Spinner) findViewById(R.id.userGender);

        List<String> list = new ArrayList<>();
        list.add("Gender");
        list.add("Male");
        list.add("Female");
        list.add("Other");
        list.add("Prefer Not to say");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        userGender.setAdapter(adapter);
        userGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (position == 0) {
                    view.setEnabled(false);
                    return;
                } else {
                    String itemvalue = adapterView.getItemAtPosition(position).toString();

                    gender = itemvalue;
//                    Toast.makeText(MakeProfileActivity.this, "Selected: " + itemvalue, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        saveInfoBtn = (Button) findViewById(R.id.saveProfileBtn);
        saveInfoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MakeProfileActivity.this, AskScheduleActivity.class);
                startActivity(intent);
            }
        });

        //saving data
        userImage = (ImageView) findViewById(R.id.userImage);
        userEmail = (EditText) findViewById(R.id.userEmail);
        userName = (EditText) findViewById(R.id.userName);
        userAddress = (EditText) findViewById(R.id.userAddress);

        firebaseAuth = FirebaseAuth.getInstance();

        if (firebaseAuth.getCurrentUser() == null) {
            //start login activity
            finish();
            startActivity(new Intent(MakeProfileActivity.this, AskScheduleActivity.class));
        }

        FirebaseUser user = firebaseAuth.getCurrentUser();

        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });

        databaseReference = FirebaseDatabase.getInstance().getReference();

        saveInfoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveUserInfo();
            }
        });
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    private void saveUserInfo() {
        //we need a java object, so we make UserInformation class
        String name = userName.getText().toString().trim();
        String address = userAddress.getText().toString().trim();
        String email = userEmail.getText().toString().trim();
        String dob = userDOB.getText().toString().trim();

        UserInformation userInformation = new UserInformation(name, email, gender, dob, address);

        FirebaseUser user = firebaseAuth.getCurrentUser();

        databaseReference.child(user.getUid()).setValue(userInformation);

        Toast.makeText(this, "Information Saved..", Toast.LENGTH_SHORT).show();
    }

    //        DateOfBirth
    public void showDialogOnButtonClick() {
        userDOB = (EditText) findViewById(R.id.userDOB);
        userDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(DIALOG_ID);
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_ID) {
            return new DatePickerDialog(this, dpickerListerner, year_x, month_x, day_x);
        } else {
            return null;
        }
    }
}
