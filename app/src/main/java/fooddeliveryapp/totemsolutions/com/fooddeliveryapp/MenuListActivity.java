package fooddeliveryapp.totemsolutions.com.fooddeliveryapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MenuListActivity extends AppCompatActivity {

    TextView plus, minus, itemQuantity;
    int quantityPerItem = 0;
    CardView menu_item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_list);

        plus = (TextView) findViewById(R.id.plus);
        minus = (TextView) findViewById(R.id.minus);
        itemQuantity = (TextView) findViewById(R.id.itemQuantity);

        menu_item = (CardView) findViewById(R.id.menu_item);
        menu_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuListActivity.this, ItemActivity.class);
                startActivity(intent);
            }
        });

        itemQuantity.setText("0");
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (quantityPerItem < 20) {
                    quantityPerItem++;
//                    Tprice = price(quantity);
//                    price.setText("Total Price: $" + Integer.toString(Tprice));
                    itemQuantity.setText(Integer.toString(quantityPerItem));
                    return;
                }
                if (quantityPerItem >= 20) {
                    Toast.makeText(MenuListActivity.this, "Maximum Quantities can be 20 only.", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (quantityPerItem > 0) {
                    quantityPerItem--;
                    itemQuantity.setText(Integer.toString(quantityPerItem));
//                    Tprice = price(quantity);
//                    price.setText("Total Price: $" + Integer.toString(Tprice));
                    return;
                }
                if (quantityPerItem <= 0) {
                    Toast.makeText(MenuListActivity.this, "Minimum Quantity is 1 only.", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });

    }
}
