package fooddeliveryapp.totemsolutions.com.fooddeliveryapp;

/**
 * Created by aakash on 2/15/2018.
 */

public class UserInformation {
    public String name;
    public String email;
    public String gender;
    public String dob;
    public String address;
//    public ImageView image;

    public UserInformation(String name, String email, String gender, String dob, String address) {
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.dob = dob;
        this.address = address;
//        this.image = image;
    }
}